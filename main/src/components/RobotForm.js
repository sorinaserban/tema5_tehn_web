import React, {Component} from 'react'

class RobotForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name : '',
            type : '',
            mass : ''
        };
        this.onAdd = (event) => {
            this.props.onAdd({
                name : this.state.name,
                type : this.state.type,
                mass : this.state.mass
            })
        };
    }
    
    handleChangeName = (event) => {
        this.setState({ name: event.target.value });
    };

    handleChangeType = (event) => {
        this.setState({ type: event.target.value });
    };

    handleChangeMass = (event) => {
        this.setState({ mass: event.target.value });
    };
    
    render() 
    {
        return (
            <div>
                <input
                    style={{ width: "150px" }}
                    onChange={this.handleChangeName}
                    placeholder="Name"
                    name="name"
                    id="name"
                    type="text"/>
                <input
                    style={{ width: "150px" }}
                    onChange={this.handleChangeType}
                    placeholder="Type"
                    name="type"
                    id="type"
                    type="text"/>
                <input
                    style={{ width: "150px" }}
                    onChange={this.handleChangeMass}
                    placeholder="Mass"
                    name="mass"
                    id="mass"
                    type="text"/>
                <input 
                    type="button"
                    value="add"
                    onClick={this.onAdd} />
            </div>
        );
    }
}
export default RobotForm